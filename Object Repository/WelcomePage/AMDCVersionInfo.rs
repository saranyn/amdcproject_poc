<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AMDCVersionInfo</name>
   <tag></tag>
   <elementGuidId>3b82d7ef-1a0f-4f19-b5d8-8e698f9a6450</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[text()='Latest News']/../following-sibling::div//div[contains(text(),'AMDC version')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
