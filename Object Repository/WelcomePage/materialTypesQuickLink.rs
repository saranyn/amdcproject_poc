<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>materialTypesQuickLink</name>
   <tag></tag>
   <elementGuidId>85caaf6e-5a2d-4929-9bea-43cc2fb598ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='Quicklinks to preferred material types:']/following-sibling::div/a[text()='${materialType}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
