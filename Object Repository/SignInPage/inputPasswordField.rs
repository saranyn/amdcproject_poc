<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputPasswordField</name>
   <tag></tag>
   <elementGuidId>f7421d84-d1e2-4df9-96c8-e058b6f7be3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#input54</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@type=&quot;password&quot; and @name=&quot;credentials.passcode&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>0c972e63-8da7-4408-8f58-3c68b7177a57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>47d26584-a265-461a-b733-7afd6a6bf9e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>credentials.passcode</value>
      <webElementGuid>0bfaaa79-0a79-479c-bf82-68fd045eba3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>input54</value>
      <webElementGuid>9b434782-8774-4a3d-8b69-8e27a6ca9c0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>06d25095-e1ab-4e87-825d-f4e2b0785819</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>password-with-toggle</value>
      <webElementGuid>63422ee1-94cb-42a2-b569-1efb6e10e166</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;input54&quot;)</value>
      <webElementGuid>f29c0e1b-f1d3-4a4e-b815-0145ff7a49b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='input54']</value>
      <webElementGuid>fdf09f20-2026-480f-84b0-aeaae7a5c5db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form46']/div/div[4]/div/div[2]/span/input</value>
      <webElementGuid>14cea5b3-371b-4576-a2a0-8da87bde9f1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>355c8a04-acff-40f2-90c5-6f9e52ebe864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @name = 'credentials.passcode' and @id = 'input54']</value>
      <webElementGuid>2e666718-c87f-4980-baac-0f45eb142733</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
