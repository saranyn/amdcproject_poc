<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>details_Material_Value2</name>
   <tag></tag>
   <elementGuidId>f3f41f18-73de-44cc-ba6c-fe0e5fc68955</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(),'Global warming potential')])[1]/following-sibling::div[@class=&quot;left text-xs w-1/6&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
