<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>details_Material_Value1</name>
   <tag></tag>
   <elementGuidId>6eb3af43-d9e0-41a7-b42b-109bcb85a117</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(text(),'Global warming potential')])[1]/following-sibling::div[@class=&quot;break-all right text-xs font-bold w-1/6 px-2&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
