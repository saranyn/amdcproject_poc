<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>firstShareItem</name>
   <tag></tag>
   <elementGuidId>d1bcceed-1e65-4ea1-9b63-a89e7d8a019c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='tile-small  flex flex-col flex-1 relative flex-1 border border-solid rounded-lg border-gray-300'][1]//i[@data-icon-name='share'][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
