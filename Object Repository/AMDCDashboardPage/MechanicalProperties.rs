<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mechanicalProperties</name>
   <tag></tag>
   <elementGuidId>21c61dc1-0902-416a-b4e5-364f158b497d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[normalize-space()='Mechanical Properties - Axial Loading']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
