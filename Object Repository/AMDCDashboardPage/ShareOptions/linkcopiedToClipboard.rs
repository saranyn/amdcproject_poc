<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>linkcopiedToClipboard</name>
   <tag></tag>
   <elementGuidId>baa77fc8-6f08-4a38-80e4-05e2a3559c43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.ms-MessageBar-innerText.innerText-368 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = 'Link copied to clipboard.' or . = 'Link copied to clipboard.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c7196e8a-4bf8-4d07-a0fb-295611e518cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Link copied to clipboard.</value>
      <webElementGuid>a495fc63-19e8-4955-a6b0-59c583f34353</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;MessageBar2257&quot;)/span[@class=&quot;ms-MessageBar-innerText innerText-368&quot;]/span[1]</value>
      <webElementGuid>e87303f9-1d4c-417f-9a22-6f9a7b4a7ab3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='MessageBar2257']/span/span</value>
      <webElementGuid>9292d2ff-c7fd-4d8c-9189-547924df42d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Metric'])[1]/following::span[5]</value>
      <webElementGuid>2bdfc4a4-9850-4ac9-bfed-10ea7bd348c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 58513 materials'])[1]/following::span[6]</value>
      <webElementGuid>3cea8912-d8da-4d2c-8789-4f6143bdbeb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New'])[1]/preceding::span[2]</value>
      <webElementGuid>bcc6376e-10d6-4a4a-8527-42aa0b8ea20c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Metal > Aluminium > 4xxx'])[1]/preceding::span[2]</value>
      <webElementGuid>ced0ce28-dd3a-4040-ae9e-b50e07bb1fe3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Link copied to clipboard.']/parent::*</value>
      <webElementGuid>50d26083-74e8-48d0-a336-fe664c2ac56c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span/span</value>
      <webElementGuid>524873e3-af8f-4972-8af0-3a24fa8b2a51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Link copied to clipboard.' or . = 'Link copied to clipboard.')]</value>
      <webElementGuid>36a00bbc-d496-4c20-a04c-77ced2581143</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
