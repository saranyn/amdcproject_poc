<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>plotElements</name>
   <tag></tag>
   <elementGuidId>4b009c4f-5be5-4235-b1bd-283f22a7a6ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg']//*[name()='g' and @class='plot']//*[name()='g' and @class='scatterlayer mlayer']//*[name()='g' and @class='points'][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
