<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>InputTextDensityValue</name>
   <tag></tag>
   <elementGuidId>7dc92ff6-76ef-4462-b23b-6d7f39c30ce5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-testid=&quot;advanced-search-scrollable-div&quot;]//div[text()='Density']//following::div[contains(@class, 'ms-TextField-fieldGroup fieldGroup')]//input[@placeholder=&quot;min&quot;])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
