<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>yieldStrength</name>
   <tag></tag>
   <elementGuidId>c0cb2909-81aa-4293-be9c-c0cb9a6d9595</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-testid=&quot;building-block-table&quot; and contains(@class,'flex flex-wrap w-full') ])[3]//div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
