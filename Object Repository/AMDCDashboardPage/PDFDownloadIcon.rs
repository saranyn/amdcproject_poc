<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pdfDownloadIcon</name>
   <tag></tag>
   <elementGuidId>7d25c403-5b63-447a-88eb-fb4fd47b0625</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i [@data-icon-name=&quot;download&quot; ]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
