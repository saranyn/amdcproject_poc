<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tableFavouriteBtn</name>
   <tag></tag>
   <elementGuidId>eb1376de-f741-4b06-9398-b598aeb021dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@title='Favorite']//span[contains(@class, 'ms-Button-flexContainer') and @data-automationid='splitbuttonprimary']//i[@data-icon-name='FavoriteStar']//i[contains(@class, 'unity_star')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
