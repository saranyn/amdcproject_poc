<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>compareClickBtn</name>
   <tag></tag>
   <elementGuidId>61a70e4a-ddf1-464e-9d4f-dba65f31dddd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[ @data-icon-name=&quot;Compare&quot; ]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
