<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DensityPoint</name>
   <tag></tag>
   <elementGuidId>10abb69e-4572-4bfa-993b-c188d4e233fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>( //*[local-name()='svg' ]//*[local-name()='g' and @class=&quot;axis&quot; ])[2]//*[local-name()=&quot;text&quot; and @text-anchor=&quot;middle&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
