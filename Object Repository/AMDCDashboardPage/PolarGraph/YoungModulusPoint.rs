<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>youngModulusPoint</name>
   <tag></tag>
   <elementGuidId>832c6a8b-d6c9-4f51-b8a5-9211311b3d88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>( //*[local-name()='svg' ]//*[local-name()='g' and @class=&quot;axis&quot; ])[1]//*[local-name()=&quot;text&quot; and @text-anchor=&quot;middle&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
