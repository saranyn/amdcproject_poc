<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>polarBtn</name>
   <tag></tag>
   <elementGuidId>f92a2583-afa5-4359-a397-8804d95c1e2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//span[contains(@class,'ms-Pivot-text text')][normalize-space()='Polar Chart'])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
