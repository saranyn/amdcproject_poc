<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>noMaterialFoundLabelName</name>
   <tag></tag>
   <elementGuidId>c3372f7c-6895-43d8-8bfa-c2c2ca04333f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-testid='building-block-paragraph-html' and contains(@class, 'text-xl') and contains(@class, 'm-2') and contains(@class, 'pt-4') and text()='No materials match your filter criteria. Please adjust.']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
