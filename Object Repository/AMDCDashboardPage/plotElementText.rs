<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>plotElementText</name>
   <tag></tag>
   <elementGuidId>bd02c7f1-2050-4fc5-9db8-d4e0311672e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg']//*[name()='g' and @class='hovertext']//*[name()='text']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
