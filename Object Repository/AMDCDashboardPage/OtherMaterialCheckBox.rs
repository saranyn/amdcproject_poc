<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>otherMaterialCheckBox</name>
   <tag></tag>
   <elementGuidId>b589a134-212f-4792-b21c-f7637d69b8ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Other materials')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
