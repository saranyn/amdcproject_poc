<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>clearFavorites</name>
   <tag></tag>
   <elementGuidId>2a531b1b-0909-40af-ad70-a493d60bb04c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.ms-ContextualMenu-itemText.label-348</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = 'Clear Favorites' or . = 'Clear Favorites')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1125f7f1-7631-4982-a9eb-9e0f088083a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ms-ContextualMenu-itemText label-348</value>
      <webElementGuid>35e6f9d1-761e-4419-b4c9-743340e9beab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Clear Favorites</value>
      <webElementGuid>b9d057fd-72c9-44d2-9ef4-b7a08a3aacdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id__282-menu&quot;)/div[@class=&quot;ms-FocusZone css-160 ms-ContextualMenu is-open ms-BaseButton-menuhost root-331&quot;]/ul[@class=&quot;ms-ContextualMenu-list is-open list-333&quot;]/li[@class=&quot;ms-ContextualMenu-item item-336&quot;]/button[@class=&quot;ms-ContextualMenu-link root-360&quot;]/div[@class=&quot;ms-ContextualMenu-linkContent linkContent-342&quot;]/span[@class=&quot;ms-ContextualMenu-itemText label-348&quot;]</value>
      <webElementGuid>c1bdfe6d-6022-4192-a21d-f9856e9fab99</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id__282-menu']/div/ul/li/button/div/span</value>
      <webElementGuid>09f55387-4ed0-46f1-8bea-aa45bd0faca6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Altair Material Data Center'])[2]/following::span[1]</value>
      <webElementGuid>29c4bfff-b2af-4386-91a2-6ba25bd0c5fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Compare'])[1]/preceding::span[1]</value>
      <webElementGuid>cddb60ed-a4e8-4ac8-85f9-56bffc54a07e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share Favorites'])[1]/preceding::span[2]</value>
      <webElementGuid>39eb3185-80e4-49b5-a150-7027bbd41e5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Clear Favorites']/parent::*</value>
      <webElementGuid>99eef701-af90-4211-ae6d-097cccf27b99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/div/span</value>
      <webElementGuid>6eefa73f-a356-4322-9bd3-32ff9b6fe267</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Clear Favorites' or . = 'Clear Favorites')]</value>
      <webElementGuid>d6a794e4-50f9-4d0e-98ca-4e1c87473d18</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
