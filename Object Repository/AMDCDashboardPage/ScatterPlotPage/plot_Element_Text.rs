<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>plot_Element_Text</name>
   <tag></tag>
   <elementGuidId>bd02c7f1-2050-4fc5-9db8-d4e0311672e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg']//*[name()='g' and @class='hovertext']//*[name()='text']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
