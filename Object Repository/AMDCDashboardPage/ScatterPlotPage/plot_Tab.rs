<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>plot_Tab</name>
   <tag></tag>
   <elementGuidId>c3f76a96-b3d0-4ef6-b586-0467efd1aea3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Pivot225-Tab1 > span.ms-Button-flexContainer.flexContainer-208 > span.ms-Pivot-linkContent.linkContent-204 > span.ms-Pivot-text.text-205</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[(text() = ' Plot' or . = ' Plot')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>58e6e7f6-df80-4d8b-bb3a-ceef31975987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ms-Pivot-text text-205</value>
      <webElementGuid>bac83208-6f39-48db-a257-493ed2aa4bdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Plot</value>
      <webElementGuid>7aef5599-2871-455e-bb6b-2a2b647d60e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Pivot225-Tab1&quot;)/span[@class=&quot;ms-Button-flexContainer flexContainer-208&quot;]/span[@class=&quot;ms-Pivot-linkContent linkContent-204&quot;]/span[@class=&quot;ms-Pivot-text text-205&quot;]</value>
      <webElementGuid>db3e0df0-5055-4d7b-ad45-c54280fde195</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='Pivot225-Tab1']/span/span/span</value>
      <webElementGuid>5d401425-4c93-4ec8-a4bd-8eb41012c250</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scatter Plot'])[1]/following::span[8]</value>
      <webElementGuid>50492bb5-fc51-4271-a6e7-878e96f2134b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Category'])[2]/preceding::span[1]</value>
      <webElementGuid>b1670ff1-7a2d-4278-948c-fd0a6a8f2455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reduce the Properties according to the Material Type'])[2]/preceding::span[4]</value>
      <webElementGuid>0ad7a7cf-324e-453d-bba8-1047593d1ec5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plot']/parent::*</value>
      <webElementGuid>647a633a-0d2a-4c92-84bb-9fc4eeb85c56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/button[2]/span/span/span</value>
      <webElementGuid>884b334b-4f09-4340-b4fe-2e5cb3d3be2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' Plot' or . = ' Plot')]</value>
      <webElementGuid>a1d800bf-4592-4ad0-8901-206c775d506d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
