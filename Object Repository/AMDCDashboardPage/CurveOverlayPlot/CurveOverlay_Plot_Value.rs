<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CurveOverlay_Plot_Value</name>
   <tag></tag>
   <elementGuidId>f6ada82f-26bc-4a35-97df-0bc3858fdc2d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg' and @class = 'main-svg']//*[name()='g' and  @class = 'scatterlayer mlayer']//*[name()='path']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
