<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>StressstrainMaterial</name>
   <tag></tag>
   <elementGuidId>64edef6e-369c-4c4d-93ea-0bcb3abee637</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//span[normalize-space()='Stress-strain (computed)'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
