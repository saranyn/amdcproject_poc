<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CurveOverlay_PlotText</name>
   <tag></tag>
   <elementGuidId>b3123f52-7464-4337-8b38-a73cc5f6077d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[local-name()='svg' and @class = 'main-svg']//*[name()='g' and @class='hovertext']//*[name()='text'] </value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
