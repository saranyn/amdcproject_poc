<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>compareDropdown</name>
   <tag></tag>
   <elementGuidId>f395554e-1037-4c1f-88d8-dc91435378ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type = &quot;button&quot; and contains(@class,&quot;ms-Button root&quot;) ]//i[@data-icon-name=&quot;ChevronDown&quot;and contains(@class,'ms-Button-menuIcon')])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
