<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>densityLabelName</name>
   <tag></tag>
   <elementGuidId>f34dba1a-d34f-454b-9fbe-18eb469ddb0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-testid=&quot;building-block-table&quot; and contains(@class,'flex flex-wrap w-full') ])[1]//div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
