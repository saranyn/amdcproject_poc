import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import java.awt.Robot as Robot
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import java.awt.event.KeyEvent as KeyEvent
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
    //WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Successfully Navigated to Material Dashboard')

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))

    extentTest.log(Status.PASS, 'QuickSearch TextBox is visible ')
	
	//As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
	String IntialSearchCount = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterialCountLbl'))

    KeywordUtil.logInfo('Before user search : ' + IntialSearchCount)

    String materialName = 'B180H1'

    WebUI.setText(findTestObject('AMDCDashboardPage/quickSearchBox'), materialName)

    WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), Keys.chord(Keys.ENTER))

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'), 5)

    String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterText)

    assert materialName == filterText

    extentTest.log(Status.PASS, 'User search material is exactly match with filtered outcome in Tile view')

    if (WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), 3)) {
        String searchText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'))

        WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), materialName)

        String materialCountAfterSearch = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterialCountLbl'))

        KeywordUtil.logInfo('User searched material count : ' + materialCountAfterSearch)

        assert IntialSearchCount != materialCountAfterSearch

        extentTest.log(Status.PASS, 'Validating material count Before search and after search both are not equal in Tile view')

        assert true
    } else {
        KeywordUtil.logInfo('Search key tile not found')

        extentTest.log(Status.FAIL, 'Search material not found')

        assert false
    }
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PDFDownloadIcon'))

    String username = System.getProperty('user.name')

    String downloadPath = ('C:\\Users\\' + username) + '\\Downloads'

    extentTest.log(Status.PASS, 'Validating wheather pdf downloaded in download folder in Table view')

    fileDownloading()

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.verifyFileDownload'(downloadPath, materialName)

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.deleteFileFromDownloads'(downloadPath, materialName)
	
	WebUI.closeWindowIndex(1)
	
	WebUI.switchToWindowIndex(0, FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'))

    WebUI.mouseOver(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/DocumentDownloadinTableview'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/DocumentDownloadinTableview'))

    extentTest.log(Status.PASS, 'Validating wheather pdf downloaded in download folder in Table view')

    fileDownloading()

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.verifyFileDownload'(downloadPath, materialName)

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.deleteFileFromDownloads'(downloadPath, materialName)
	
	WebUI.closeWindowIndex(1)
	
	WebUI.switchToWindowIndex(0, FailureHandling.STOP_ON_FAILURE)
	
	//revert back to normal
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed, Window title not as expected')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution Completed ')
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	 //	uncomment when executing as testcase
     //	WebUI.closeBrowser()
}

def fileDownloading() {
    Robot robot = new Robot()

    String url = WebUI.getUrl()

    KeywordUtil.logInfo('Title of the Tab : ' + url)

    String getTitle = WebUI.getWindowTitle()

    KeywordUtil.logInfo('Title of the Tab : ' + getTitle)

    WebUI.delay(5)

    robot.keyPress(KeyEvent.VK_CONTROL)

    Thread.sleep(100)

    robot.keyPress(KeyEvent.VK_S)

    Thread.sleep(100)

    robot.keyRelease(KeyEvent.VK_S)

    Thread.sleep(100)

    robot.keyRelease(KeyEvent.VK_CONTROL)

    Thread.sleep(100)

    System.out.println('Waiting for 2 seconds before pressing Enter...')

    Thread.sleep(2000)

    robot.keyPress(KeyEvent.VK_ENTER)

    robot.keyRelease(KeyEvent.VK_ENTER)
}