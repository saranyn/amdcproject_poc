import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
//    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'), 20)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/material_Selection'), 5)
	
	extentTest.log(Status.PASS, 'Verified Material Selection')
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))
	

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))
	
	//As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.quickSearch'('B180H1')
	
	extentTest.log(Status.PASS, 'quick search successfull for the given material')

    WebDriver driver = DriverFactory.getWebDriver()

    List<WebElement> actualMaterialDataList = new ArrayList()

    List<WebElement> materialDataList = driver.findElements(By.xpath('(//div[@data-testid="building-block-table"]//div[contains(@class,"w-full text-xs font-small font-semibold")])'))

    for (WebElement materialData : materialDataList) {
        String materials = materialData.getText().replaceAll('[^0-9.E-]+', '')

        actualMaterialDataList.add(materials)
    }
    
    KeywordUtil.logInfo('List of data from material : ' + actualMaterialDataList)
	
	extentTest.log(Status.PASS, 'Getting material Data in  a list')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/MaterialDetails'))
	
	extentTest.log(Status.PASS, 'performing click action on material details')

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/propertiesTab'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/propertiesTab'))
	
	extentTest.log(Status.PASS, 'User able to access the properties of a material')

    String densityvalue = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/DensityValue'))

    KeywordUtil.logInfo('density value  : ' + densityvalue)
	

    String youngsmodulusValue = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/youngModulus'))

    KeywordUtil.logInfo('youngsmodulus Value  :' + youngsmodulusValue)

    String poissonsRatio = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/poissonsRatio'))

    KeywordUtil.logInfo('poissonsRatio Value  :' + poissonsRatio)

    String yieldStrength = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/yieldStrength'))

    KeywordUtil.logInfo('yieldStrength value : ' + yieldStrength)

    String ultimateTensileStrength = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/ultimateTensileStrength'))

    KeywordUtil.logInfo('ultimateTensileStrength value : ' + ultimateTensileStrength)

    List<WebElement> expectedMaterialDataList = Arrays.asList(densityvalue, youngsmodulusValue, poissonsRatio, yieldStrength, 
        ultimateTensileStrength)

    KeywordUtil.logInfo('expected result : ' + expectedMaterialDataList)
	
	extentTest.log(Status.PASS, 'Adding each property value into expectedmaterialdatalist')

    assert actualMaterialDataList == expectedMaterialDataList
	
	extentTest.log(Status.PASS, 'validating material data list with actual vs expected')
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution is completed')

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter' //uncomment below line to run as single test case
            ) //WebUI.closeBrowser()
        )
}