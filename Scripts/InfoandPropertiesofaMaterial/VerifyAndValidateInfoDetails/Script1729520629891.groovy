import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
//    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

    extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'), 20)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/material_Selection'), 5)
	
	extentTest.log(Status.PASS, 'Verified Material Selection')
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))
	

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))
	
	//As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
	String materialName = 'B180H1'

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.quickSearch'(materialName)

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/MaterialDetails'), FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'material details opened')

    materialName2 = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterailDINName'))
	
	extentTest.log(Status.PASS, 'material details opened fetching material id name from the info')

    KeywordUtil.logInfo('material name : ' + materialName2)

    assert materialName == materialName
	
	extentTest.log(Status.PASS, 'validating the material name')

    material_Type = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterailDINName'))

    KeywordUtil.logInfo('material type  : ' + material_Type)

	extentTest.log(Status.PASS, 'Getting the material type')
	
    material_Type_FromTile = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/metalTypeFromTile'))

    KeywordUtil.logInfo('material type from tile : ' + material_Type_FromTile)

    assert "Low-Alloyed" in material_Type_FromTile
 
	extentTest.log(Status.PASS, 'validating material type')
	
    disclaimerText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/disclaimerText'))

    KeywordUtil.logInfo('disclaimerText : ' + disclaimerText)
	
	extentTest.log(Status.PASS, 'getting disclaimer text')
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution is completed')

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter' //uncomment below line to run as single test case
            //WebUI.closeBrowser()
            ))
}