import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
    //	WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Navigating to Material Dashboard page')

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))

    //As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
    //Table view Navigated
    extentTest.log(Status.PASS, 'checking Table view')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    //Clearing all favourite
    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.clear_Favourite'()

    String actualCountText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/favouritesCount'))

    KeywordUtil.logInfo('actualCountText ' + actualCountText)

    String expectedCountText = CustomKeywords.'reusuablekeywordspackage.reusablekeywords.getFavTextCount'('0')

    KeywordUtil.logInfo('Expected Count Text :  ' + expectedCountText)

    extentTest.log(Status.PASS, 'Validating Favourite count is 0 ')

    assert expectedCountText == actualCountText

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TableFavouriteBtn'))

    //click on favourite
    extentTest.log(Status.PASS, 'clicking on favourite btn')

    String expectedCountTextNew = CustomKeywords.'reusuablekeywordspackage.reusablekeywords.getFavTextCount'('1')

    String actualCountTextNew = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/favouritesCount'))

    KeywordUtil.logInfo('actualCountTextNew ' + actualCountTextNew)

    KeywordUtil.logInfo('expected Count Text : ' + expectedCountTextNew)

    extentTest.log(Status.PASS, 'Validating Favourite count is 1 ')

    //validating the favourite count
    assert expectedCountTextNew == actualCountTextNew

    //clear the favourite
    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.clear_Favourite'()
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution is completed')

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter') //uncomment below line to run as single test case
        ) //WebUI.closeBrowser()
}