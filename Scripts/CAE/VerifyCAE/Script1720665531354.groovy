import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebElement
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),  20)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/CAE_Applications'),5)
	
	extentTest.log(Status.PASS, "CAE Applications tile is present on dashboard")
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/CAE_Applications'))
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 40)
	
	extentTest.log(Status.PASS, "CAE Applications opened")
	
	//Verify Filter added
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'),10)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/softwareFilterLabel'),5)
	
	extentTest.log(Status.PASS, "Filter section added")
	
	WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/softwareFilterLabel'), 3)
	
	//Verify all filter items under Software are checked
	TestObject testObject = findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItems',['filterBy':'Software'])
	
	int count = WebUI.findWebElements(testObject,5).size()
	System.out.println("size:"+count)
	
	for(int i=1; i<=count; i++) {
		WebUI.delay(2)	
		WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemCheckBox',['filterBy':'Software','index':i]), 5)	
		WebUI.verifyElementChecked(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemCheckBox',['filterBy':'Software','index':i]), 10)
	}
	
	extentTest.log(Status.PASS, "All filter items under Software are checked")
	
	//Verify individual Quick filters
	def softwaresList = ['CADFEKO','FLUX','HyperLife','Inspire','Inspire Extrude Polymer','Inspire Form','Inspire Mold','OptiStruct','Radioss','SimLab']
	for (String software:softwaresList) {
		WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/softwareQuickLink',['software':software]),20)
		WebUI.click(findTestObject('Object Repository/WelcomePage/softwareQuickLink',['software':software]))
		WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 40)
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'),5)
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/searchResultsContent'),5)
		String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))
		assert filterText.contains(software)
		
		extentTest.log(Status.PASS, "Data filtered for Quick filter: "+software)
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/softwareFilterLabel'), 20)
		WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemSpecificCheckBox',['filterBy':'Software','filterItem':software]), 3)
		WebUI.verifyElementChecked(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemSpecificCheckBox',['filterBy':'Software','filterItem':software]),3)	
		extentTest.log(Status.PASS, software+" filter checkbox is checked under Software")
	}
}
catch (StepFailedException se) {	
	   extentTest.log(Status.FAIL, 'Test case validation Failed')
	
	   WebUI.takeScreenshot(screenShotPath)
	   
	   extentTest.log(Status.FAIL, se.getMessage())
	
	   extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	   
	   KeywordUtil.markFailed("Test case validation Failed")
   }
catch (AssertionError ae) {
	extentTest.log(Status.FAIL, 'Assertion Failed for Filter text validation')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, ae.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {	
	WebUI.takeScreenshot(screenShotPath)
	
    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
} finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}