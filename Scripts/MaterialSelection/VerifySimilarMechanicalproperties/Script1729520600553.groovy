import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.testobject.ConditionType as ConditionType

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    //uncomment below line to run as single test case
//    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'), 20)

    WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/material_Selection'), 5)

    extentTest.log(Status.PASS, 'Verified Material Selection')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Clicked on Material Selection')

    WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 20)
	
	//As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
    extentTest.log(Status.PASS, 'searching for 0120 material is success')

    CustomKeywords.'reusuablekeywordspackage.reusablekeywords.quickSearch'('0120')

    extentTest.log(Status.PASS, 'check into Table view')

    WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'))

    WebUI.mouseOver(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/vennDiagramTableView'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/vennDiagramTableView'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/similarMechanicalProperties'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/similarMechanicalProperties'))

    TestObject filterText = new TestObject()

    filterText.addProperty('xpath', ConditionType.EQUALS, '(//span[contains(@class,\'filterButtonText\')])[2]')

    String data = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    data.contains('Similar Mechanical Properties')
	
	extentTest.log(Status.PASS, 'showing similar mechanical properties', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/mechanicalpropertiesContentRows'))
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/ScoreRowElement'))
	
	extentTest.log(Status.PASS, 'showing similar mechanical properties')
	
    TestObject closeBtn = new TestObject().addProperty("xpath", ConditionType.EQUALS, '(//i[@data-icon-name="ChromeClose"]//i)[2]')
	
	WebUI.enhancedClick(closeBtn)
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/CompareClickBtn'), 10)
	
	WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))
	
	WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/CompareDropdown'))
	
	WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/ClearSection')  )
	
	
}
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution Completed!')

    // Going to Landing page
   WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter')) //uncomment below line to run as single test case
    //WebUI.closeBrowser()
}