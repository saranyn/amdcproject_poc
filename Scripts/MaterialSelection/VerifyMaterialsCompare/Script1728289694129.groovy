import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')

	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),  20)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/material_Selection'),5)
	
	extentTest.log(Status.PASS, "Verified Material Selection")
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))
	
	extentTest.log(Status.PASS, "Clicked on Material Selection")
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 20)

	//Select material 1
	WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/materialNameInList',['materialName':material1]), 3)	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/materialCheckBox',['materialName':material1]))	
	extentTest.log(Status.PASS, 'Selected Material1 - '+material1)
	
	//Select material 2
	WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/materialNameInList',['materialName':material2]), 3)	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/materialCheckBox',['materialName':material2]))	
	extentTest.log(Status.PASS, 'Selected Material2 - '+material2)
	
	//Click Compare button
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CompareClickBtn'))
	extentTest.log(Status.PASS, 'Clicked on Compare button')
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/compareSectionLbl'), 3)
	extentTest.log(Status.PASS, 'Compare section is Opened')
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/showDifferencesOnlyLbl'))
	extentTest.log(Status.PASS, 'Clicked on Show differences only checkbox')
	
	//Check Comparison Details
	List differecesList = WebUI.findWebElements(findTestObject('Object Repository/AMDCDashboardPage/differenceList'), 3)
	for(int i=1; i<=differecesList.size(); i++) {
		String material1Text = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/material1CompareValue',['index':i]))
		String material2Text = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/material2CompareValue',['index':i]))
		if(material1Text!='' || material1Text!='') {
			WebUI.verifyNotEqual(material1Text,material2Text)
		}
	}
	extentTest.log(Status.PASS, 'Materials comparison results validated')
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))

}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, ex.getMessage())

	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}
	