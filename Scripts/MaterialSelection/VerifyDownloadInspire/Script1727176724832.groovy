import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))	
	extentTest.log(Status.PASS, "Clicked on Material Selection")
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))
	
	//Search with Matrial ID
	WebUI.setText(findTestObject('AMDCDashboardPage/quickSearchBox'), materialId)
	
	WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), Keys.chord(Keys.ENTER))	
	extentTest.log(Status.PASS, "Searched with Material: "+materialId)
	
	WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), materialId)
	
	//Open details
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'))	
	extentTest.log(Status.PASS, "Clicked on Material title to open details")
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/materialCAEModelTab'))	
	extentTest.log(Status.PASS, "Clicked on CAE model tab")
	
	//Select Inspire
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/selectSoftwareDropDown'))
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/softwareDropDownValue',['value':software]))
	
	extentTest.log(Status.PASS, "Selected Software '"+software+"' from Software dropdown")
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/downloadButton'))
	
	extentTest.log(Status.PASS, "Clicked on Download button")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/downloadSuccessMessage'), 25)
	
	extentTest.log(Status.PASS, "Download success message is displayed")
	
	String downloadPath = (System.getProperty('user.home') + '\\Downloads')
	WebUI.delay(3)
	boolean downloadStatus = CustomKeywords.'reusuablekeywordspackage.reusablekeywords.verifyFileDownload'(downloadPath, downloadedFileName)
	if(!downloadStatus) {
		extentTest.log(Status.FAIL,"File Download Failed" )
		KeywordUtil.markFailed("Download Failed")
	}
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.deleteFileFromDownloads'(downloadPath, downloadedFileName)
}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {	
	WebUI.takeScreenshot(screenShotPath)
	
    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
} 
finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}