import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, "Browser opened and login is successful")
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),  20)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/ecologicalTopics'),5)
	
	extentTest.log(Status.PASS, "Ecological Topics tile is present on landing page")
	
	WebUI.click(findTestObject('Object Repository/WelcomePage/ecologicalTopics'))
	
	extentTest.log(Status.PASS, "Clicked on Ecological Topics tile in landing page")
	
	WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 20)
	
	extentTest.log(Status.PASS, "AMDC Dashboard page opened")
	
	//Verify Materials loaded with Ecological Topics Filters
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'),20)
	
	extentTest.log(Status.PASS, "Filter added")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/searchResultsContent'),5)
	
	String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))
	assert filterText.contains("Biodegradable and 2 more")
	
	extentTest.log(Status.PASS, "Filter section contains Biodegradable and 2 more text")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/ecologicalTopicsFilterLabel'),5)
	
	WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/softwareFilterLabel'), 3)
	
	WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/ecologicalTopicsFilterLabel'))
	
	//Verify Filter items under Ecological Topics are checked
	TestObject testObject = findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItems',['filterBy':'Ecological Topics'])
	
	int count = WebUI.findWebElements(testObject,5).size()
	System.out.println("size:"+count)
	
	for(int i=1; i<=count; i++) {
		WebUI.verifyElementChecked(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemCheckBox',['filterBy':'Ecological Topics','index':i]), 3)
	}
	
	extentTest.log(Status.PASS, "All filter items under Ecological Topics are checked")

}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {	
	WebUI.takeScreenshot(screenShotPath)
	
    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}  finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}
