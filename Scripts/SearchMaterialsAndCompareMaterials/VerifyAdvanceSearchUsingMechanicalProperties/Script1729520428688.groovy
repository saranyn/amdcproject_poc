import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
//    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Successfully Navigated to Material Dashboard')

     //As a precondition checking we are in tile view
	
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/OtherMaterialCheckBox'))

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'), 5)

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/searchResultsContent'), 5)

    String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    println('Filter Text :' + filterText)

    assert filterText.contains('Other materials')

    extentTest.log(Status.PASS, ' Other materail Filter keyword word visible')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/advanceSearchBtn'))

    WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/advanceSearchLabel'), 'Advanced Search')

    extentTest.log(Status.PASS, 'Advance search label is visible')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/selectCategoryDropdown'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/physicalPropertiesCheckBox'))

    extentTest.log(Status.PASS, 'Physicalproperties are selected')

    WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/selectCategoryDropdown'))

    WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/PhyscialPropertiesDropdown'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/DenisityLabel'))

    extentTest.log(Status.PASS, 'Density label is visible for set text')

    String denisity = '5.38E-9'

    WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/InputTextDensityValue'), denisity)

    WebUI.sendKeys(findTestObject('/AdvancedSearch/input_Material_Name'), Keys.chord(Keys.ENTER))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/propertiesTab'))

    data = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/DensityForOtherMaterial'), FailureHandling.STOP_ON_FAILURE)

    println('Data density :' + data)

    assert denisity == data

    extentTest.log(Status.PASS, 'Denisity Data is equal with DensityforOthermaterial')
}
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed,Expected Result is not equal with the actual result')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution Completed!')

    //revert back to normall
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	 //uncomment below line to run as single test case
    //WebUI.closeBrowser()
}