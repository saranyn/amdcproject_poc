import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

materialID = 'B180H1'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
    //WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Navigating to Material Dashboard page')

    //As a precondition checking we are in tile view
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/advanceSearchBtn'))

    WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/advanceSearchLabel'), 'Advanced Search')

    extentTest.log(Status.PASS, 'Validating Advance search')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/selectCategoryDropdown'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/physicalPropertiesCheckBox'))

    extentTest.log(Status.PASS, 'click on physical properties')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/generalInfoProductCheckBox'))

    extentTest.log(Status.PASS, 'click on general info checkbox')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/advanceSearchLabel'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/generalInformationMetalDropdown'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/designDropdown'))

    extentTest.log(Status.PASS, 'click on design dropdown')

    WebUI.delay(1)

    WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/input_Material_Name'), materialID)

    WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/AdvancedSearch/input_Material_Name'), Keys.chord(
            Keys.ENTER))

    extentTest.log(Status.PASS, 'searching for material name')

    if (WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), 3)) {
        String searchText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'))

        WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), materialID)

        extentTest.log(Status.PASS, 'search result validation')

        assert true
    } else {
        println('Search key tile not found')

        assert false
    }
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution is completed')

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter')) //uncomment below line to run as single test case
    //WebUI.closeBrowser()
}