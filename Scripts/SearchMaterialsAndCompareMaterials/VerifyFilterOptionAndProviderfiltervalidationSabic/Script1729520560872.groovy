import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.console.ui.BytecodeCollector as BytecodeCollector
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import java.util.List as List
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
    //    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Navigating to Material Dashboard page')

     //As a precondition checking we are in tile view
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()
	
    filterOptionProviderAndvalidation('Sabic')

    WebUI.refresh()

    WebUI.delay(3)

    filterOptionProviderAndvalidation('Salzgitter')
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution is completed')

    // Going to Landing page
    WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter' //uncomment below line to run as single test case
            ) //WebUI.closeBrowser()
        )
}
//Filtering the data based on given providername
//validating the material info plot and properties CAE model

def filterOptionProviderAndvalidation(def providername) {
    def extentTest = GlobalVariable.G_ExtentTest

    WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderDropdown'), 30)

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderDropdown'))

    WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderSearchBtn'), providername)

    extentTest.log(Status.PASS, 'Click on given providername')

    if (providername == 'Salzgitter') {
        TestObject object = new TestObject()

        object.addProperty('xpath', ConditionType.EQUALS, '(//span[text()=\'Salzgitter\']/..)[2]')

        WebUI.verifyElementPresent(object, 5)

        WebUI.click(object)
    } else {
        WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderCheckBoxForBaoSteel', 
                [('providername') : providername]), 5)

        WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderCheckBoxForBaoSteel', [('providername') : providername]))
    }
    
    String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterText)

    assert filterText == providername

    extentTest.log(Status.PASS, 'For the given provider material list is sorted')

    WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), 3)

    WebDriver driver = DriverFactory.getWebDriver()

    List<WebElement> materialsList = driver.findElements(By.xpath('//button[contains(@class,\'border-none bg-none cursor-pointer\')]'))

    if (!(materialsList.isEmpty())) {
        materialsList.get(0).click()

        extentTest.log(Status.PASS, 'For the given provider material list clicking on one of the material')
    } else {
        KeywordUtil.logInfo('No elements found with the specified class.')
    }
    
    validation()

    extentTest.log(Status.PASS, 'Validation of material info and properties and plot and CAE modes visible and accessiable')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    extentTest.log(Status.PASS, 'Naviagating to Table view')

    String filterText2 = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterText2)

    assert filterText2 == providername

    extentTest.log(Status.PASS, 'For the given provider material list is sorted')

    TestObject dynamicObject = new TestObject()

    dynamicObject.addProperty('xpath', ConditionType.EQUALS, '//div[@data-selection-index=\'1\']')

    WebUI.doubleClick(dynamicObject)

    extentTest.log(Status.PASS, 'click on the material for the Table view')

    validation()

    extentTest.log(Status.PASS, 'Validation of material info and properties and plot and CAE modes visible and accessiable')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))

    extentTest.log(Status.PASS, 'clear All filters')

    WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))
}

def validation() {
    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/materialLabelName'))

    String materialText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/materialLabelName'))

    KeywordUtil.logInfo('material name : ' + materialText)

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/Info'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/propertiesTab'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/Plots'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/CAEModel'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Info'))

    String materialIDName = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterailDINName'))

    KeywordUtil.logInfo('material name : ' + materialIDName)

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/propertiesTab'))

    String density = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/DensityLabelName'))

    KeywordUtil.logInfo('density labelname : ' + density)

    String value = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/Propertiess/DensityValue'))

    KeywordUtil.logInfo('density value  : ' + value)

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Plots'))

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/PlotGraph'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CAEModel'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CloseBtn'))
}