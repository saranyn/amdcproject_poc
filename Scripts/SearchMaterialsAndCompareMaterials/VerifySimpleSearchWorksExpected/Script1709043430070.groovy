import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.util.concurrent.ConcurrentHashMap.KeySetView as KeySetView
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.networknt.schema.CustomErrorMessageType as CustomErrorMessageType
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'

try {
    extentTest.log(Status.PASS, 'Test started')

    //uncomment below line to run as single test case
    //    WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
    extentTest.log(Status.PASS, 'Browser opened and login is successful')

    WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

    extentTest.log(Status.PASS, 'Navigating to Material Dashboard page')

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))

    extentTest.log(Status.PASS, 'QuickSearch TextBox is visible ')

     //As a precondition checking we are in tile view
	CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()

    String IntialSearchCount = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterialCountLbl'))

    KeywordUtil.logInfo('Before user search : ' + IntialSearchCount)

    KeywordUtil.logInfo('material id : ' + materialID)

    String materialName = materialID

    WebUI.setText(findTestObject('AMDCDashboardPage/quickSearchBox'), materialName)

    WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), Keys.chord(Keys.ENTER))

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'), 5)

    String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterText)

    assert materialName == filterText

    extentTest.log(Status.PASS, 'User search material is exactly match with filtered outcome in Tile view')

    //Tile view Keyword search is visible
    if (WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), 3)) {
        String searchText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'))

        WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), materialName)

        String materialCountAfterSearch = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterialCountLbl'))

        KeywordUtil.logInfo('User searched material count : ' + materialCountAfterSearch)

        assert IntialSearchCount != materialCountAfterSearch

        extentTest.log(Status.PASS, 'Validating material count Before search and after search both are not equal in Tile view')

        assert true
    } else {
        KeywordUtil.logInfo('Search key tile not found')

        extentTest.log(Status.FAIL, 'Search material not found')

        assert false
    }
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))

    extentTest.log(Status.PASS, 'Cleared All Filter')

    //Table view keyword Search visible
    WebUI.setText(findTestObject('AMDCDashboardPage/quickSearchBox'), materialName)

    extentTest.log(Status.PASS, 'Searching Material name From Table view')

    WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), Keys.chord(Keys.ENTER))

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'), 5)

    String filterTextTableView = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterTextTableView)

    assert materialName == filterTextTableView

    extentTest.log(Status.PASS, 'User search material is exactly match with filtered outcome in Table view')

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    if (WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'), 
        3)) {
        String searchText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'))

        WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromTableList'), materialName)

        String materialCountinTableView = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/MaterialCountLbl'))

        KeywordUtil.logInfo('User searched material count : ' + materialCountinTableView)

        assert IntialSearchCount != materialCountinTableView

        extentTest.log(Status.PASS, 'Validating material count Before search and after search both are not equal in Table view')

        assert true
    } else {
        KeywordUtil.logInfo('Search key tile not found')

        extentTest.log(Status.FAIL, 'Search material not found')

        assert false
    }
    
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))

    extentTest.log(Status.PASS, 'Clearing All filter is Tile view')

    //revert back to Tile view
    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))

    //Search invalid material
    WebUI.setText(findTestObject('AMDCDashboardPage/quickSearchBox'), 'b180ha')

    WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), Keys.chord(Keys.ENTER))

    WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'), 5)

    String filterresult = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))

    KeywordUtil.logInfo('Filter Text :' + filterresult)

    assert filterresult == 'b180ha'

    WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/NoMaterialFoundLabelName'))

    WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))
}
catch (AssertionError ae) {
    extentTest.log(Status.FAIL, 'Assertion Failed,Expected Result is not equal with the actual result')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (StepFailedException se) {
    extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, se.getMessage())

    extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

    KeywordUtil.markFailed('Test case validation Failed')
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution completed')

    // Going to Landing page
    WebUI.enhancedClick(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter' //	WebUI.closeBrowser()
            ))
}