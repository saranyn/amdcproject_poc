import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {

    //uncomment below line to run as single test case
    //WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
    extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
    WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)

    // Get the current window handles
    WebDriver driver = DriverFactory.getWebDriver()
	
	String currentWindow = driver.getWindowHandle()

    def windowHandlesBefore = driver.getWindowHandles()

    WebUI.scrollToElement(findTestObject('Object Repository/WelcomePage/onlineHelpLink'), 5)

    WebUI.click(findTestObject('Object Repository/WelcomePage/onlineHelpLink'))

    extentTest.log(Status.PASS, 'Clicked on Online help link')

    //Verify Online Help page
    WebUI.delay(5)
	
    // Get the window handles after the new window is opened
    def windowHandlesAfter = driver.getWindowHandles()

    // Verify that the number of windows has increased
    assert windowHandlesAfter.size() == (windowHandlesBefore.size() + 1) : 'New window not opened'

    // Switch to the new window
    String newWindowHandle = (windowHandlesAfter - windowHandlesBefore).iterator().next()

    driver.switchTo().window(newWindowHandle)

    extentTest.log(Status.PASS, 'Switched to New window')

    String newWindowTitle = WebUI.getWindowTitle()

    System.out.println("New Window title: "+newWindowTitle)

    //Verify Window title
    assert newWindowTitle.equalsIgnoreCase('Tutorials')
	
	extentTest.log(Status.PASS, 'Online help page is Opened')

    extentTest.log(Status.PASS, 'Verified Online help page title')
	
	driver.close()
	
	driver.switchTo().window(currentWindow)

}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (AssertionError ae) {
	extentTest.log(Status.FAIL, 'Assertion Failed, Online help Window title not as expected')

	WebUI.takeScreenshot(screenShotPath)
	
    extentTest.log(Status.FAIL, ae.getMessage())

    extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {
    WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
} 
finally { 
    extentTest.log(Status.PASS, 'Test Execution Completed!')
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}