import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/welcomeNote'),5)
	
	//Verify Landing page Title
	String title = WebUI.getWindowTitle()
	System.out.println("Window Title:"+title)
	assert title.equalsIgnoreCase("Altair Material Data Center")
	
	extentTest.log(Status.PASS, "Verified Landing page title")
	
	//Verify Landing page tiles
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),5)
	
	extentTest.log(Status.PASS, "Verified AMDC logo")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/CAE_Applications'),5)
	
	extentTest.log(Status.PASS, "Verified CAE Applications")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/material_Selection'),5)
	
	extentTest.log(Status.PASS, "Verified Material Selection")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/ecologicalTopics'),5)
	
	extentTest.log(Status.PASS, "Verified Ecological Topics")
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/zieglerPEMData'),5)
	
	extentTest.log(Status.PASS, "Verified Ziegler PEM Data")
	
	//Verify Quicklinks to preferred software
	def softwaresList = ['CADFEKO','FLUX','HyperLife','Inspire','Inspire Extrude Polymer','Inspire Form','Inspire Mold','Inspire Print 3D','OptiStruct','Radioss','SimLab','SimLab-Injection Molding','SimSolid']
	for (String software:softwaresList) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/softwareQuickLink',['software':software]),5)
	}
	extentTest.log(Status.PASS, "Verified Quicklinks to Preferred Software from Landing page")
	
	//Verify Quicklinks to preferred material types
	def materialTypesList = ['Metals','Plastics Generic','Plastics Trade Products','Adhesives','Fibers','Glasses','Polymer Additives','Structural Foams']
	for (String type:materialTypesList) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]),5)
	}
	extentTest.log(Status.PASS, "Verified Quicklinks to Preferred Material Types from Landing page")
	
	//Verify Quicklinks to preferred material compatibility
	def materialCompatibilityList = ['Good','Medium','Poor']
	for (String materialCompatibility:materialCompatibilityList) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/materialCompatibilityQuickLink',['materialCompatibility':materialCompatibility]),5)
	}
	extentTest.log(Status.PASS, "Verified Quicklinks to Preferred Material Compatibility from Landing page")
	
	//Verify Quicklinks to preferred materials
	def materialList = ['Biodegradable','Contains renewable resources','Recycled resin content']
	for (String material:materialList) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/materialQuickLink',['material':material]),5)
	}
	extentTest.log(Status.PASS, "Verified Quicklinks to Preferred Materials from Landing page")	
}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (AssertionError ae) {
	extentTest.log(Status.FAIL, 'Assertion Failed for Landing page title')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, ae.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {	
	WebUI.takeScreenshot(screenShotPath)
	
    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
} 
finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}