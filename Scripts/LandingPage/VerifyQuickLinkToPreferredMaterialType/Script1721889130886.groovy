import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),  20)
	
	//Verify quick links for Metals, Fibers
	def materialTypesList = ['Metals','Fibers']
	
	for (String type:materialTypesList) {
	
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]),5)
		
		extentTest.log(Status.PASS, type+' Material Type is Present')
		
		WebUI.scrollToElement(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]), 3)
		
		WebUI.click(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]))
		
		extentTest.log(Status.PASS, 'Clicked on quick link of Material Type-'+type)
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 20)
		
		//Verify Filter added
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'),5)
		
		extentTest.log(Status.PASS, 'Filter added for Material Type-'+type)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/searchResultsContent'),5)
		
		String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))
		assert filterText.contains(type)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/materialTypeFilterLabel'),5)
		
		WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/materialTypeFilterLabel'), 3)
		
		WebUI.verifyElementChecked(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemSpecificCheckBox',['filterBy':'Material Type','filterItem':type]), 3)
		
		extentTest.log(Status.PASS, type+' Material type checkbox is Checked')
		
		WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
		
		extentTest.log(Status.PASS, 'Going back to Landing page')
	}
	
	//Verify quick links for Plastics Generic,Plastics Trade Products
	def providerList = ['Plastics Generic','Plastics Trade Products']
	
	for (String type:providerList) {
	
		WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]),5)
		
		extentTest.log(Status.PASS, type+' Material Type is Present')
		
		WebUI.scrollToElement(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]), 3)
		
		WebUI.click(findTestObject('Object Repository/WelcomePage/materialTypesQuickLink',['materialType':type]))
		
		extentTest.log(Status.PASS, 'Clicked on quick link of Material Type-'+type)
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), 20)
		
		//Verify Filter added
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/filterButton'),5)
		
		extentTest.log(Status.PASS, 'Filter added for Material Type-'+type)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/searchResultsContent'),5)
		
		String filterText = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/filterButtonText'))
		assert filterText.contains(type)
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/providerFilterLabel'),5)
		
		WebUI.scrollToElement(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/providerFilterLabel'), 3)
		
		WebUI.verifyElementChecked(findTestObject('Object Repository/AMDCDashboardPage/FiltersSection/filterItemSpecificCheckBox',['filterBy':'Provider','filterItem':type]), 3)
		
		extentTest.log(Status.PASS, type+' Material type checkbox is Checked')
		
		WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
		
		extentTest.log(Status.PASS, 'Going back to Landing page')
	}
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, ex.getMessage())

	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}