import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.aventstack.extentreports.Status as Status
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepFailedException

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = ((RunConfiguration.getProjectDir() +'/ExtentReports/' + TestCaseName) + GlobalVariable.G_Browser) + '.png'

try {	
	//uncomment below line to run as single test case
	//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	
	extentTest.log(Status.PASS, 'Browser opened and login is successful')
	
	WebUI.waitForElementPresent(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'),  20)
	
	//Verify Landing page title
	String title = WebUI.getWindowTitle()
	System.out.println("Title:"+title)
	assert title.equalsIgnoreCase("Altair Material Data Center")
	
	extentTest.log(Status.PASS, "Verified Landing page title")
	
	//Verify Welcome heading
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/welcomeNote'),5)
	
	extentTest.log(Status.PASS, "Verified Welcome heading")
	
	//Verify Welcome Description
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/welcomeNote_description'),5)
	String actualWelcomeDescription = WebUI.getText(findTestObject('Object Repository/WelcomePage/welcomeNote_description'))
	System.out.println("Actual Description:"+actualWelcomeDescription)
	System.out.println("Expected Description:"+GlobalVariable.G_WelcomeDescription_Paragraph1)
	assert actualWelcomeDescription.contains(GlobalVariable.G_WelcomeDescription_Paragraph1)
	assert actualWelcomeDescription.contains(GlobalVariable.G_WelcomeDescription_Paragraph2)
	extentTest.log(Status.PASS, "Verified Welcome note Description")
	
	//Verify AMDC Version in Landing page
	WebUI.verifyElementPresent(findTestObject('Object Repository/WelcomePage/AMDCVersionInfo'),5)
	String actualVersion = WebUI.getText(findTestObject('Object Repository/WelcomePage/AMDCVersionInfo'))
	System.out.println("Version:"+actualVersion)
	assert actualVersion.contains(GlobalVariable.G_AMDC_Version)
	extentTest.log(Status.PASS, "Verified AMDC version")
}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')
 
	WebUI.takeScreenshot(screenShotPath)
	
	extentTest.log(Status.FAIL, se.getMessage())
 
	extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
}
catch (AssertionError ae) {
 extentTest.log(Status.FAIL, 'Assertion Failed..')

 WebUI.takeScreenshot(screenShotPath)
 
 extentTest.log(Status.FAIL, ae.getMessage())

 extentTest.log(Status.FAIL,"Screenshot attached", MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
 
 KeywordUtil.markFailed("Test case validation Failed")
}
catch (Exception ex) {
	
	WebUI.takeScreenshot(screenShotPath)

    extentTest.log(Status.FAIL, 'in catch block')

    extentTest.log(Status.FAIL, ex.getMessage())

    extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())
	
	KeywordUtil.markFailed("Test case validation Failed")
} 
finally {
	extentTest.log(Status.PASS, 'Test Execution Completed!')	
	
	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter'))
	
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}