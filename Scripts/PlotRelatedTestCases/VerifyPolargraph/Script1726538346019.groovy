import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.aventstack.extentreports.MediaEntityBuilder as MediaEntityBuilder
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.exception.StepFailedException as StepFailedException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.aventstack.extentreports.Status as Status
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ConditionType

// Initialize the Extent Report
def extentTest = GlobalVariable.G_ExtentTest

//Set Screeenshot path
String screenShotPath = (((RunConfiguration.getProjectDir() + '/ExtentReports/') + TestCaseName) + GlobalVariable.G_Browser) + 
'.png'


try {
extentTest.log(Status.PASS, 'Test started')

//uncomment below line to run as single test case
//WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

extentTest.log(Status.PASS, 'Browser opened and login is successful')

WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))

extentTest.log(Status.PASS, 'Successfully Navigated to Material Dashboard')

WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'))

extentTest.log(Status.PASS, 'QuickSearch TextBox is visible ')

//As a precondition checking we are in tile view

CustomKeywords.'reusuablekeywordspackage.reusablekeywords.checkAndSwitchTileView'()

WebDriver d = DriverFactory.getWebDriver()

Actions actions = new Actions(d)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderDropdown'))

extentTest.log(Status.PASS, 'Choosing provider as Baosteel ')

WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderSearchBtn'), 'BaoSteel')

WebUI.verifyElementPresent(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderCheckBoxForBaoSteel', [('providername') : 'BaoSteel']), 
    5)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderCheckBoxForBaoSteel', [('providername') : 'BaoSteel']))


List<WebElement> elements = d.findElements(By.xpath('//div[@class="flex flex-col items-center"]//label[contains(@class,"ms-Checkbox-label label-")]//div//i'))

if (elements.size() >= 3) {
    elements.get(1).click()

    elements.get(2).click()
}

extentTest.log(Status.PASS, 'performing click action on compare button ')

WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/CompareClickBtn'))

extentTest.log(Status.PASS, 'performing click action polar chart ')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/PolarBtn'))

extentTest.log(Status.PASS, 'checking properties for polar graph ')

extentTest.log(Status.PASS, 'performing click action Density checkbox ')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/DensityCheckBox'))

extentTest.log(Status.PASS, 'performing click action young modulus')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/YoungModulusCheckBox'))

extentTest.log(Status.PASS, 'performing click action yield strength')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/YieldStrength'))

extentTest.log(Status.PASS, 'performing click action polar chart ')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/PolarChartClickBtn'))

extentTest.log(Status.PASS, 'Checking Density point visible on graph')

WebUI.mouseOver(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/densityPolarGraph'))

WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/PolarToolTipValue'), 10)

String result = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/PolarToolTipValue'))

println('result : ' + result)

String keyword = result.split('\n')[1]

String textvaluefromgraph = result.split('\n')[0]

println('text value : ' + textvaluefromgraph)

println('Keyword search :' + keyword)

CustomKeywords.'reusuablekeywordspackage.reusablekeywords.quickSearch'(keyword)

textfrommaterial = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/DensityTxt'))

println('text value : ' + textfrommaterial)

extentTest.log(Status.PASS, 'Checking Density point visible on graph')

WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/DensityPoint'))


extentTest.log(Status.PASS, 'Checking YStrengthPoint visible on graph')

WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/YStrengthPoint'))

extentTest.log(Status.PASS, 'Checking YStrengthPoint visible on graph')

WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/PolarGraph/YoungModulusPoint'))

extentTest.log(Status.PASS, 'Checking YoungModulus visible on graph')

extentTest.log(Status.PASS, 'validating graph value with the material value',MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

assert textvaluefromgraph == textfrommaterial


TestObject closeBtn = new TestObject().addProperty("xpath", ConditionType.EQUALS, '(//i[@data-icon-name="ChromeClose"]//i)[2]')


WebUI.enhancedClick(closeBtn)

WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/CompareClickBtn'), 10)

WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/ClearAllFilter'))

WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/CompareDropdown'))

WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/ClearSection')  )

}

catch (AssertionError ae) {
	extentTest.log(Status.FAIL, 'Assertion Failed')

	WebUI.takeScreenshot(screenShotPath)

	extentTest.log(Status.FAIL, ae.getMessage())

	extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

	KeywordUtil.markFailed('Test case validation Failed')
}
catch (StepFailedException se) {
	extentTest.log(Status.FAIL, 'Step Failed Unable Validate')

	WebUI.takeScreenshot(screenShotPath)

	extentTest.log(Status.FAIL, se.getMessage())

	extentTest.log(Status.FAIL, 'Screenshot attached', MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

	KeywordUtil.markFailed('Test case validation Failed')
}
catch (Exception ex) {
	WebUI.takeScreenshot(screenShotPath)

	extentTest.log(Status.FAIL, 'in catch block')

	extentTest.log(Status.FAIL, ex.getMessage())

	extentTest.log(MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build())

	KeywordUtil.markFailed('Test case validation Failed')
}
finally {
	extentTest.log(Status.PASS, 'Test Execution is completed')

	// Going to Landing page
	WebUI.click(findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter')) 
	//uncomment below line to run as single test case
	//WebUI.closeBrowser()
}