import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.inspect.swingui.BytecodeCollector
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor


//uncomment below line to run as single test case
WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

WebDriver d = DriverFactory.getWebDriver()



WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderDropdown'))

WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderSearchBtn'), 'BaoSteel')

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/Provider/ProviderCheckBoxForBaoSteel'))

List elements = d.findElements(By.xpath('//div[contains(@class, "ms-Checkbox is-enabled ml-auto root")]'))

if (elements.size() >= 3) {
	elements.get(1).click()

	elements.get(2).click()
}

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CompareClickBtn'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/CurveOverlayBtn'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/SearchDropdown'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/StressstrainMaterial'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/ProductCheckBox'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/XcoordinateCheckBox'))

// Mouse over the element to trigger the tooltip
WebUI.mouseOver(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/CurveOverlay_Plot_Value'))

Actions actions = new Actions(d)

// Find the SVG element
WebElement element = d.findElement(By.xpath("(//*[local-name()='svg' and @class = 'main-svg']//*[name()='g' and  @class = 'y'])[2]//*[1]"))





// Perform mouse actions using Actions class

actions.moveToElement(element).moveByOffset(0, 0) // Adjust the offset as needed
	   .release()
	   .build()
	   .perform()

// Wait for the tooltip element to be present
WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/CurveOverlay_PlotText'), 10)

String value = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/CurveOverlayPlot/CurveOverlay_PlotText'))

println("plot Element Tooltip: "+value)