import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement as Keys
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

//uncomment below line to run as single test case
WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/scatter_Plot'))

WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/ecological_Properties'), 10)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/ecological_Properties'))

WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/global_Warming_Potential_Component_X'), 10)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/global_Warming_Potential_Component_X'))

WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/global_Warming_Potential_Component_Y'), 10)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/global_Warming_Potential_Component_Y'))

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/plot_Tab'))

WebUI.mouseOver(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/plot_Elements'))

String text = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/plot_Element_Text'))

println("plot Element Tooltip: "+text)

String materialName = CustomKeywords.'reusuablekeywordspackage.reusableKeywordsForScatterPlot.getMaterialName'(text, "0")
String global_warming_potential_value = CustomKeywords.'reusuablekeywordspackage.reusableKeywordsForScatterPlot.getMaterialName'(text, "1")

assert text.contains("Global warming potential:")

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/ScatterPlotPage/scatter_Plot_Close_Button'))

CustomKeywords.'reusuablekeywordspackage.reusablekeywords.quickSearch'(materialName)
WebUI.verifyElementText(findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList'), materialName)

String index = CustomKeywords.'reusuablekeywordspackage.reusableKeywordsForScatterPlot.getIndexOfSearchMaterialFromList'(materialName)	
println(index)
TestObject detailsBtnObject = CustomKeywords.'reusuablekeywordspackage.reusableKeywordsForScatterPlot.createTestObjectForDetailsButton'(index)
println(detailsBtnObject)

WebUI.click(detailsBtnObject)

WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/DetailsPage/properties_tab'))

String value1 = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/DetailsPage/details_material_value1'))

String value2 = WebUI.getText(findTestObject('Object Repository/AMDCDashboardPage/DetailsPage/details_material_value2'))

String actualMaterialValue = value1 + " " + value2

println(actualMaterialValue)

assert actualMaterialValue == global_warming_potential_value


