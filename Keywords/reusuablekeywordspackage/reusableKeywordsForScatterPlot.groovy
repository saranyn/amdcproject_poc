package reusuablekeywordspackage

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By




public class reusableKeywordsForScatterPlot {


	@Keyword
	def getMaterialName(String text, String flag) {

		String[] str = text.split("Global warming potential: ~")

		String materialName = str[0]
		String global_warming_potential_value = str[1]

		println(materialName)
		println(global_warming_potential_value)
		if(flag == "0")
			return materialName
		else
			return global_warming_potential_value
	}

	@Keyword
	def createTestObjectForDetailsButton(String index) {
		String xpath = '(//button[contains(@title,"Details")])['+index+']'
		println xpath
		// Building detailsBtn indentifier obj
		TestObject detailsBtnXpath = new TestObject('objectName')
		detailsBtnXpath.addProperty('xpath', ConditionType.EQUALS, xpath)
		return detailsBtnXpath
	}

	def getIndexOfSearchMaterialFromList(String materialName) {

		TestObject ele = findTestObject('Object Repository/AMDCDashboardPage/materialTextFromList')
		List<WebElement> eleText = WebUI.findWebElements(ele, 10)
		println(eleText.size())
		for (int i = 1;  i <= eleText.size(); ++i) {
			String xpath = '(//button[contains (@class,"text-altair-500 font-bold text-base")])['+i+']'
			println xpath
			// Building materialFromList indentifier obj
			TestObject materialTextBtnXpath = new TestObject('objectName')
			materialTextBtnXpath.addProperty('xpath', ConditionType.EQUALS, xpath)
			String mName = WebUI.getText(materialTextBtnXpath)
			println(mName)
			if (mName == materialName) {
				return String.valueOf(i)
			}
		}
		return null
	}






}
