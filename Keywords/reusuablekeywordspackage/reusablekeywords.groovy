package reusuablekeywordspackage
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.JavascriptExecutor


import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.Keys as Keys
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class reusablekeywords {
	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUI.findWebElement(to)
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUI.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}


	@Keyword
	def signInAndNavigateToAmdcDashboard(String username, String password) {

		WebUI.setText(findTestObject('Object Repository/SignInPage/inputUsernameTextField'), username)
		WebUI.click(findTestObject('Object Repository/SignInPage/nextButton'))
		WebUI.setEncryptedText(findTestObject('Object Repository/SignInPage/inputPasswordField'), password)
		WebUI.click(findTestObject('Object Repository/SignInPage/verifyButton'))
		WebUI.verifyElementVisible( findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter') )
		WebUI.waitForElementClickable(findTestObject('Object Repository/WelcomePage/close_PopUp_Button'), 60)
		WebUI.click(findTestObject('Object Repository/WelcomePage/close_PopUp_Button'))
		//WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))
	}

	@Keyword
	def signInFromLandingPage(String username, String password) {

		WebUI.waitForElementClickable(findTestObject('Object Repository/WelcomePage/close_PopUp_Button'), 60)
		WebUI.click(findTestObject('Object Repository/WelcomePage/close_PopUp_Button'))
		//WebUI.click(findTestObject('Object Repository/WelcomePage/material_Selection'))
		WebUI.click(findTestObject('Object Repository/WelcomePage/SignInBtn'))
		WebUI.setText(findTestObject('Object Repository/SignInPage/inputUsernameTextField'), username)
		WebUI.click(findTestObject('Object Repository/SignInPage/nextButton'))
		WebUI.setEncryptedText(findTestObject('Object Repository/SignInPage/inputPasswordField'), password)
		WebUI.click(findTestObject('Object Repository/SignInPage/verifyButton'))
		WebUI.verifyElementVisible( findTestObject('Object Repository/WelcomePage/AltairMaterialDataCenter') )
	}

	@Keyword
	def createTestObjectForFavouriteButton(String materialName) {
		String str = "'"+materialName+"'"
		String xpath = '//button[(text()='+str+')]//following::button[@title="Favorite"]'
		KeywordUtil.logInfo xpath
		// Building favBtn indentifier obj
		TestObject favBtnXpath = new TestObject('objectName')
		favBtnXpath.addProperty('xpath', ConditionType.EQUALS, xpath)
		return favBtnXpath
	}

	@Keyword
	def getFavTextCount(String count) {
		String favText = 'Favorites ('+count+')'
		return favText
	}

	@Keyword
	def quickSearch(String searchKeyword) {
		WebUI.setText(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'), searchKeyword)
		WebUI.sendKeys(findTestObject('Object Repository/AMDCDashboardPage/quickSearchBox'),Keys.chord(Keys.ENTER))
	}

	@Keyword
	def clear_Favourite() {

		WebUI.waitForElementPresent(findTestObject('Object Repository/AMDCDashboardPage/favoritesDownChevron'), 4)
		WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/favoritesDownChevron'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/clearFavorites'), 4)
		WebUI.enhancedClick(findTestObject('Object Repository/AMDCDashboardPage/clearFavorites'))
	}

	@Keyword
	def verifyFileDownload(String downloadDir, String fileName) {
		KeywordUtil.logInfo("Download Directory: " + downloadDir)
		KeywordUtil.logInfo("File Name: " + fileName)
		Path filePath = Paths.get(downloadDir, fileName)
		WebUI.delay(2)
		// Check if the file exists
		if (Files.exists(filePath)) {

			KeywordUtil.logInfo("File downloaded successfully: " + filePath.toString())
			return true
		} else {
			KeywordUtil.logInfo("File not found: " + filePath.toString())
			return false
		}
	}


	@Keyword
	def deleteFileFromDownloads(String downloadDir, String fileName) {
		Path filePath = Paths.get(downloadDir, fileName)

		// Check if the file exists
		if (Files.exists(filePath)) {
			try {
				// Delete the file
				Files.delete(filePath)
				KeywordUtil.logInfo("File deleted successfully: " + filePath.toString())
				return true
			} catch (IOException e) {
				KeywordUtil.logInfo("Failed to delete the file: " + e.message)
				return false
			}
		} else {
			KeywordUtil.logInfo("File not found: " + filePath.toString())
			return false
		}
	}

	@Keyword
	def checkAndSwitchTileView() {
		boolean tileView = WebUI.waitForElementVisible(findTestObject('Object Repository/AMDCDashboardPage/SingleTileLocator'), 5)
		if (!tileView) {
			WebUI.click(findTestObject('Object Repository/AMDCDashboardPage/TileorTableView'))
			WebUI.verifyElementVisible(findTestObject('Object Repository/AMDCDashboardPage/SingleTileLocator'))
			WebUI.takeScreenshot()
		}
	}
}


