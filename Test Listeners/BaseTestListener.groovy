import java.text.SimpleDateFormat

import org.junit.After
import org.openqa.selenium.Capabilities
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.events.EventFiringWebDriver

import com.assertthat.selenium_shutterbug.utils.web.Browser
import com.aventstack.extentreports.ExtentReports
import com.aventstack.extentreports.MediaEntityBuilder
import com.aventstack.extentreports.Status
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import com.aventstack.extentreports.ReactiveSubject

import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

class BaseTestListener {
	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */

    def LocalDateTime endTime	
	def reportFolder=(RunConfiguration.getReportFolder()+'/')
	def totalTime
	def test
	def startTime = LocalDateTime.now()
	
	String execID = RunConfiguration.getExecutionSourceName()
	def Browser= GlobalVariable.G_Browser
	def browserVersion= GlobalVariable.G_Browser_Version
	def filePath = (RunConfiguration.getProjectDir() + '/reports/'+execID+Browser+'.txt')

	def reportFliePath=(RunConfiguration.getProjectDir() + '/ExtentReports/')

	def date = new Date()
	def sdf = new SimpleDateFormat("ddMMyyyy_HHmmss")
	def execTime = sdf.format(date)
	String execTag=Browser+'_'+execTime

	def ReportFile =execID+'_'+execTag+'.html'
	
	ExtentReports extent = CustomKeywords.'com.report.GenerateReport.createSpark'(ReportFile,Browser ,browserVersion , totalTime)	
	
	@BeforeTestSuite
	def BeforeTestSuite(TestSuiteContext testSuiteContext)
	{	
		println ("From Brefore Suite")
		println(RunConfiguration.getReportFolder())
		
		//Calling SignIn Testcase
		WebUI.callTestCase(findTestCase('SignInTestCase'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@BeforeTestCase
	def BeforeTestCase(TestCaseContext testCaseContext) {
		String variableList=testCaseContext.getTestCaseVariables()
		println("context - "+variableList)
		println(testCaseContext.getTestCaseId())
		def tcID=testCaseContext.getTestCaseId()

		String [] tcScript=tcID.split('\\/')
		def len=tcScript.size()
		def tcScriptName=tcScript[len-1]
		println("==========================")
		println(tcScriptName)
		println("==========================")

		GlobalVariable.G_ExtentTest = extent.createTest(tcScriptName)

		def extentTest=GlobalVariable.G_ExtentTest

		/*try {
			WebDriver driver = DriverFactory.getWebDriver()
			Capabilities caps =((RemoteWebDriver) (((EventFiringWebDriver) driver).getWrappedDriver())).getCapabilities()
			def bn= caps.getBrowserName()
			def bv = caps.getVersion()
		}
		catch (Exception ex) {
			String screenShotPath = ((reportFolder + ex) + GlobalVariable.G_Browser) + '.png'
			WebUI.takeScreenshot(screenShotPath)
			String p = (ex + GlobalVariable.G_Browser) + '.png'
			extentTest.log(Status.FAIL, ex)
			extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(p).build())
			System.out.println("In test List:"+extentTest)
		}*/
	}

	@AfterTestSuite
	def AfterTestSuite()
	{				
		endTime = LocalDateTime.now();
		println ("*****************************************************************")
		Duration duration = Duration.between(startTime, endTime)
		long minutes = duration.toMinutes();
		long seconds = duration.minusMinutes(minutes).getSeconds();
				
		totalTime = String.format("%d minutes and %d seconds", minutes, seconds);
		System.out.println(totalTime);
		extent.setSystemInfo("Total Duration", totalTime);
		println("After Suite ")
		println ("*****************************************************************")
		println ("Report location - "+reportFliePath)
		extent.flush()
		WebUI.closeBrowser()
	}
	@AfterTestCase
	void AfterTestCase() {
		//extentTest.log(Status.PASS, 'Closing the browser after executinge test case - ' + TestCaseName)
		extent.flush()
		
	}
	

}